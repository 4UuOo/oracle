# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

学号：202010414118    姓名：王等等    班级：2020级1班

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。

- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。

- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。

- 提交时间： 2023-5-26日前

  ## 主要内容

  #### 设计方案

  1. 表及表空间设计方案

     该系统有以下四张表：

     - 商品表：包含商品ID、名称、价格、库存等信息。
     - 订单表：包含订单ID、下单时间、购买商品ID、购买数量等信息。
     - 用户表：包含用户ID、姓名、地址、电话等信息。
     - 支付表：包含支付ID、支付时间、订单ID、支付金额等信息。
  
     将数据分为两个表空间：数据表空间和索引表空间。
  
     - 数据表空间：包含商品表、订单表、用户表的数据文件。
     - 索引表空间：包含以上三张表的索引文件。

  2.  权限及用户分配方案

     我们至少需要两个用户：管理员和普通用户。

     管理员具备所有权限，普通用户只能查询商品和下单购买。

     创建用户的SQL语句如下：

     ```sql
     -- 创建管理员用户
     CREATE USER admin IDENTIFIED BY 112233;
     
     -- 创建普通用户
     CREATE USER customer IDENTIFIED BY 123;
     
     ```
  
     为用户分配角色和权限的SQL语句如下：
  
     ```sql
     -- 创建管理员角色
     CREATE ROLE admin_role;
     
     -- 为管理员角色授权
     GRANT ALL PRIVILEGES TO admin_role;
     
     -- 将管理员用户分配到管理员角色
     GRANT admin_role TO admin;
     
     -- 将普通用户分配到PUBLIC角色，仅授权查询商品表
     GRANT SELECT ON 商品表 TO PUBLIC;
     
     ```

  3. 存储过程和函数

     - 查询商品：根据商品ID或名称查询商品信息。

     - 下单购买：将订单信息插入订单表，并更新商品表中的库存信息。

     - 查询订单：根据订单ID或用户ID查询订单信息。
  
       程序包的SQL代码如下：
  
       ```sql
       CREATE OR REPLACE PACKAGE sales_pkg IS
         -- 查询商品信息
         FUNCTION find_product(p_id NUMBER, p_name VARCHAR2) RETURN SYS_REFCURSOR;
       
         -- 下单购买
         PROCEDURE create_order(p_product_id NUMBER, p_quantity NUMBER, p_user_id NUMBER);
       
         -- 查询订单信息
         FUNCTION find_order(p_order_id NUMBER, p_user_id NUMBER) RETURN SYS_REFCURSOR;
       END sales_pkg;
       /
       
       CREATE OR REPLACE PACKAGE BODY sales_pkg IS
         -- 查询商品信息
         FUNCTION find_product(p_id NUMBER, p_name VARCHAR2) RETURN SYS_REFCURSOR IS
           result SYS_REFCURSOR;
         BEGIN
           IF p_id IS NOT NULL THEN
             -- 根据商品ID查询商品信息
             OPEN result FOR SELECT * FROM 商品表 WHERE 商品ID = p_id;
           ELSE
             -- 根据商品名称查询商品信息
             OPEN result FOR SELECT * FROM 商品表 WHERE 商品名称 = p_name;
           END IF;
           RETURN result;
         END find_product;
       
         -- 下单购买
         PROCEDURE create_order(p_product_id NUMBER, p_quantity NUMBER, p_user_id NUMBER) IS
           v_price NUMBER;
           v_name VARCHAR2(100);
         BEGIN
           -- 查询商品价格和名称
           SELECT 商品名称, 商品价格 INTO v_name, v_price FROM 商品表 WHERE 商品ID = p_product_id;
       
           -- 新增订单
           INSERT INTO 订单表 (订单ID, 下单时间, 商品ID, 购买数量, 购买单价, 用户ID)
           VALUES (SEQ_ORDER.NEXTVAL, SYSDATE, p_product_id, p_quantity, v_price, p_user_id);
       
           -- 更新商品库存
           UPDATE 商品表 SET 库存 = 库存 - p_quantity WHERE 商品ID = p_product_id;
         END create_order;
       
         -- 查询订单信息
         FUNCTION find_order(p_order_id NUMBER, p_user_id NUMBER) RETURN SYS_REFCURSOR IS
           result SYS_REFCURSOR;
         BEGIN
           -- 根据订单ID或用户ID查询订单信息
           OPEN result FOR
             SELECT o.订单ID, o.下单时间, o.购买数量, o.购买单价, o.商品ID, p.商品名称, p.商品价格, o.用户ID
             FROM 订单表 o
             JOIN 商品表 p ON o.商品ID = p.商品ID
             WHERE o.订单ID = p_order_id OR o.用户ID = p_user_id;
           RETURN result;
         END find_order;
       END sales_pkg;
       /
       ```
  
       
  
  4. 数据库备份方案
  
     - 定期备份：每周进行一次全量备份，每天进行一次增量备份。

     - 备份数据存储：将备份文件存储到独立的存储设备上，以防止数据丢失。

     - 测试备份恢复：定期进行备份恢复测试，以确保备份可用。
  
       备份脚本代码如下：
  
       ```sql
       -- 全量备份脚本
       expdp user/123 full=y directory=backup_dir dumpfile=full_%U.dmp logfile=expdp_full.log
       
       -- 增量备份脚本
       expdp user/123 directory=backup_dir dumpfile=incr_%U.dmp logfile=expdp_incr.log tables=商品表,订单表,用户表,支付表
       
       -- 备份恢复脚本
       impdp user/123 directory=backup_dir dumpfile=full_1.dmp,incr_1.dmp logfile=impdp.log table_exists_action=REPLACE
       
       ```
  
       

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|