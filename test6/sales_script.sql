-- 创建数据表空间
CREATE TABLESPACE data
DATAFILE 'F:\oracledata\data.dbf'
SIZE 100M
AUTOEXTEND ON
NEXT 100M
MAXSIZE UNLIMITED;

-- 创建索引表空间
CREATE TABLESPACE data_index
DATAFILE 'F:\oracledata\index.dbf'
SIZE 50M
AUTOEXTEND ON
NEXT 50M
MAXSIZE UNLIMITED;

-- 创建商品表
CREATE TABLE 商品表 (
  商品ID NUMBER(10) PRIMARY KEY,
  商品名称 VARCHAR2(100) NOT NULL,
  商品价格 NUMBER(10, 2) NOT NULL,
  库存 NUMBER(10) NOT NULL
)
TABLESPACE data;

-- 创建订单表
CREATE TABLE 订单表 (
  订单ID NUMBER(10) PRIMARY KEY,
  下单时间 TIMESTAMP DEFAULT SYSDATE,
  购买商品ID NUMBER(10) NOT NULL,
  购买数量 NUMBER(10) NOT NULL,
  购买单价 NUMBER(10, 2) NOT NULL,
  用户ID NUMBER(10) NOT NULL
)
TABLESPACE data;

-- 创建用户表
CREATE TABLE 用户表 (
  用户ID NUMBER(10) PRIMARY KEY,
  姓名 VARCHAR2(100) NOT NULL,
  地址 VARCHAR2(200) NOT NULL,
  电话 VARCHAR2(20) NOT NULL
)
TABLESPACE data;

-- 创建支付表
CREATE TABLE 支付表 (
  支付ID NUMBER(10) PRIMARY KEY,
  支付时间 TIMESTAMP DEFAULT SYSDATE,
  订单ID NUMBER(10) NOT NULL,
  支付金额 NUMBER(10, 2) NOT NULL
)
TABLESPACE data;

-- 创建管理员用户
CREATE USER admin IDENTIFIED BY 112233
DEFAULT TABLESPACE data
TEMPORARY TABLESPACE temp;

-- 创建普通用户
CREATE USER user IDENTIFIED BY 123
DEFAULT TABLESPACE data
TEMPORARY TABLESPACE temp;

-- 创建管理员角色
CREATE ROLE admin1_role;

-- 为管理员角色授权
GRANT ALL PRIVILEGES TO admin1_role;

-- 将管理员用户分配到管理员角色
GRANT admin1_role TO admin;

-- 将普通用户分配到PUBLIC角色，仅授权查询商品表
GRANT SELECT ON 商品表 TO PUBLIC;

-- 创建存储过程
CREATE OR REPLACE PACKAGE sales_pkg IS
  -- 查询商品信息
  FUNCTION find_product(p_id NUMBER, p_name VARCHAR2) RETURN SYS_REFCURSOR;

  -- 下单购买
  PROCEDURE create_order(p_product_id NUMBER, p_quantity NUMBER, p_user_id NUMBER);

  -- 查询订单信息
  FUNCTION find_order(p_order_id NUMBER, p_user_id NUMBER) RETURN SYS_REFCURSOR;
END sales_pkg;
/

CREATE OR REPLACE PACKAGE BODY sales_pkg IS
  -- 查询商品信息
  FUNCTION find_product(p_id NUMBER, p_name VARCHAR2) RETURN SYS_REFCURSOR IS
    result SYS_REFCURSOR;
  BEGIN
    IF p_id IS NOT NULL THEN
      -- 根据商品ID查询商品信息
      OPEN result FOR SELECT * FROM 商品表 WHERE 商品ID = p_id;
    ELSE
      -- 根据商品名称查询商品信息
      OPEN result FOR SELECT * FROM 商品表 WHERE 商品名称 = p_name;
    END IF;
    RETURN result;
  END find_product;

  -- 下单购买
  PROCEDURE create_order(p_product_id NUMBER, p_quantity NUMBER, p_user_id NUMBER) IS
    v_price NUMBER;
    v_name VARCHAR2(100);
  BEGIN
    -- 查询商品价格和名称
    SELECT 商品名称, 商品价格 INTO v_name, v_price FROM 商品表 WHERE 商品ID = p_product_id;

    -- 新增订单
    INSERT INTO 订单表 (订单ID, 下单时间, 商品ID, 购买数量, 购买单价, 用户ID)
    VALUES (SEQ_ORDER.NEXTVAL, SYSDATE, p_product_id, p_quantity, v_price, p_user_id);

    -- 更新商品库存
    UPDATE 商品表 SET 库存 = 库存 - p_quantity WHERE 商品ID = p_product_id;
  END create_order;

  -- 查询订单信息
  FUNCTION find_order(p_order_id NUMBER, p_user_id NUMBER) RETURN SYS_REFCURSOR IS
    result SYS_REFCURSOR;
  BEGIN
    -- 根据订单ID或用户ID查询订单信息
    OPEN result FOR
      SELECT o.订单ID, o.下单时间, o.购买数量, o.购买单价, o.商品ID, p.商品名称, p.商品价格, o.用户ID
      FROM 订单表 o
      JOIN 商品表 p ON o.商品ID = p.商品ID
      WHERE o.订单ID = p_order_id OR o.用户ID = p_user_id;
    RETURN result;
  END find_order;
END sales_pkg;
/

-- 定期备份脚本
expdp user/123 full=y directory=backup_dir dumpfile=full_%U.dmp logfile=expdp_full.log

-- 增量备份脚本
expdp user/123 directory=backup_dir dumpfile=incr_%U.dmp logfile=expdp_incr.log tables=商品表,订单表,用户表,支付表

-- 备份恢复脚本
impdp user/123 directory=backup_dir dumpfile=full_1.dmp,incr_1.dmp logfile=impdp.log table_exists_action=REPLACE
